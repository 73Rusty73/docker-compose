FROM  python:3.6

COPY ./resourses /usr/src/app/

WORKDIR /usr/src/app

RUN pip install --no-cache-dir -r requirements.txt && rm -rf /var/cache/apt && rm -rf /tmp && chmod u+x ./entrypoint.sh

ENTRYPOINT ["/bin/bash", "./entrypoint.sh"]
